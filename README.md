Repository with Code/Sample Data for the paper - ***"A Machine Learning Understanding of Sepsis"*** - Under submission @ EMBC 2021

### Repo Directory Structure
```
.
├── Data
│   ├── Sample_Data.xlsx - Sample data to run interpretability notebook (6 examples of patients for interpretability notebook)                 
│   └── Sepsis_Preprocessed_Full.xlsx - A preprocessed dataset of 80 parameters recorded for 800 patiens 
├── Model-Interpretations.ipynb - A notebook to load the model+interpreter and visualize model explanations
├── Models
│   ├── ensemble_gb_rf.pkl - Ensemble Random Forest + Gradient Boosting classifier for "Sepsis Severity"
│   └── lime_explainer - LIME model explainer 
├── Scripts
│   ├── data_imputation.py - Code for regression imputation
│   ├── data_preprocessing.py - Code for label encoding of categorical variables
│   ├── models.py - ML models used in the paper
│   ├── train.py - Training+Eval script with 5-fold Cross    Validation
│   └── train_ensemble.py - Training all combinations of models using a voting classifier
├── README.md
└── requirements.txt
```

### Dataset Description

`./Data/Sepsis_Preprocessed_Full.xlsx` - A dataset of 80 parameters for 800 patients, collected and provided by [Amrita  Institute of Medical Sciences](https://www.amritahospitals.org/) (AIMS).

![alt text for the image](./Figures/fig-1.png)
![alt text for the image](./Figures/fig-2.png)

This dataset can be used to reproduce results shared in the paper and also for other future analyses. 

### To Setup ###
> pip install -r requirements.txt


### Understanding Model Explanations

1. Open `Model-Interpretations.ipynb` using jupyter notebook. 
2. Set the settings in Cell #7 - `number of features` and `number of explanations`
3. Run the entire notebook.

The individual figures represent explanations of the prediction of sepsis severity for a single patient. The result shows the probability of prediction for each class. It also shows the features that either motivated or demotivated the model to predict a particular label (with weights). Please refer to the paper's Section 5 for more details. 

### Who do I talk to? ###

* Manish Shetty - mmshetty.98@gmail.com
* Gowri Srinivasa - gsrinivasa@pes.edu