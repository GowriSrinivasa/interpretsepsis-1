import pandas as pd
import numpy as np
import pickle
import json

# encode categorical data
def process_categorical_columns(data, column):

    # label encoding by finding unique values for each column
    mapping = {}
    count = 0
    for value in data[column].unique():
        if(value == 'NA'):
            mapping['NA'] = -1
        else:
            mapping[value] = count
        count += 1

    data["encoded " + column] = data[column].map(mapping)
    data.drop([column], axis=1, inplace=True) #drop the original column

    # print(f"Label Encoded {column}")

    return data, mapping