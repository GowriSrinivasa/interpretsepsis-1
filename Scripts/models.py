from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.svm import LinearSVC 
from sklearn.metrics import classification_report, confusion_matrix
import xgboost as xgb

def RandomForest(X_train, y_train):
    print('training a RandomForestClassifier ')
    random_forest = RandomForestClassifier(n_estimators=300).fit(X_train, y_train) 
    return random_forest

def Linear_SVC(X_train, y_train):
    print('training a LinearSVC classifier ')
    svm_linear = LinearSVC(max_iter=5000).fit(X_train, y_train) 
    return svm_linear

def AdaBoost(X_train, y_train):
    print('training an Adaboost classifier ')
    ada_boost = AdaBoostClassifier(n_estimators=100, algorithm='SAMME').fit(X_train,y_train)
    return ada_boost

def GradientBoost(X_train, y_train):
    print('training an Gradientboosting classifier ')
    grad_boost = GradientBoostingClassifier(n_estimators=100, learning_rate=0.1).fit(X_train,y_train)
    return grad_boost
